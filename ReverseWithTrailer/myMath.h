float calc_magnitude(float *v) {
  return(sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]));
}

float mean(float *arr,int len) {
  float mean = 0.0f;
  for(int ii=0;ii<len;ii++) {
    mean += arr[ii];
  }
  return(mean/len);
}

float mystd(float *arr,int len) {
  float arr_mean = mean(arr,len);
  float sum = 0.0f,temp_diff=0.0f;
  for(int ii=0;ii<len;ii++) {
    temp_diff = arr[ii]-arr_mean;
    sum += temp_diff*temp_diff;
  }
  return(sqrt(sum/len));
}

float mymax(float *arr, int len) {
  float max_v = arr[0];
  for(int ii=1;ii<len;ii++) {
    if(arr[ii] > max_v) {
      max_v = arr[ii];
    }
  }
  return(max_v);
}

int any(uint8_t *arr,int len) {
  for(int ii=0;ii<len;ii++) {
    if(arr[ii])
      return(1);
  }
  return(0);
}

