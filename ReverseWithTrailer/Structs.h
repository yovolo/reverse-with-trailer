typedef struct Buffer {
  float *array;
  int ind;
  int len;
  float mean;
  float std;
} Buffer;

typedef struct Color {
  uint8_t r;
  uint8_t g;
  uint8_t b;
} Color;

typedef struct StepperMotor {
  uint8_t enable;
  int Accumulator;
  int Velocity;
  volatile uint8_t *dir_port;
  volatile uint8_t *step_port;
  volatile uint8_t *enable_port;
  uint8_t dir_pin;
  uint8_t step_pin;
  uint8_t enable_pin;
  uint8_t bit_mask;
} StepperMotor;

typedef struct Joint {
  float trailer_angle;
  int angle_offset;
  float theta_cmd;
  float Lc;
  float Lt;
  float K;
} Joint;

typedef struct Control {
  uint8_t mode; // 0-manual 1- auto
  float speed_cmd;     // speed command from user
  float steer_cmd;     // steer command from user
  float theta_cmd;     // control rule output
  int trailer_num;
  float trailer_angle; // measured angle between links
  float Ks; // control gain
  float Wc;  // distance between car wheels
  float Lc;  // car length
  float Lt;  // trailer length
  float Line_Kp;
  float Line_Kd;
} Control;

typedef struct InMessage {
  //uint8_t *data;
  //int len;
  int steer_cmd;
  int speed_cmd;
  int mode;
  int update_offset;
} InMessage;

typedef struct DebugMessage {
  uint8_t *data;
  int steer_cmd;
  int speed_cmd;
  int mode;
} DebugMessage;

uint32_t ConvertColor(Color rgb, float weight) {
  float w = weight;
  if(w > 1.0f) {
    w = 1.0f;
  }
  if(w < 0) {
    w = 0;
  }
  uint32_t r = uint32_t(w*float(rgb.r))<<16;
  uint32_t g = uint32_t(w*float(rgb.g))<<8;
  uint32_t b = uint32_t(w*float(rgb.b));
  return(r+g+b);
  //return(uint32_t(float(rgb.r))<<16+uint32_t(float(rgb.g))<<8 + uint32_t(float(rgb.b)));
  //return(0xffffff);
}
