#include <TimerOne.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <avr/pgmspace.h>
#include <string.h>
#include <EEPROM.h>
#include <util/delay.h>
#include "Structs.h"
#include "myMath.h"
#include "Buffer.h"

#define sbi(x,y) (x |= (1<<y))
#define cbi(x,y) (x &= ~(1<<y))
#define tbi(x,y) (x ^= (1<<y))

#define TRAILER_NUM 1
#define STEER_DEADBAND 0.01
#define SPEED_DEADBAND 10
#define SPEED2STEPPER  5000.5f
#define MSG_SPEED_FACTOR 0.0005;
#define MSG_SPEED_OFFSET 512
#define MSG_STEER_FACTOR 0.002;
#define MSG_STEER_OFFSET 512
#define MSG_LEN_IN 13
#define ABS(X) (X>0?X:-X)

typedef struct timer {
	int acc;
	int pre;
	uint8_t dn;
	uint8_t en;
} timer;

float calc_magnitude(float *v);
float mean(float *arr,int len);
float mystd(float *arr,int len);
float mymax(float *arr, int len);
int any(uint8_t *arr,int len);
float tf_filter(float *coef_b, float *coef_a, Buffer *input, Buffer *output,int len);
float rho_t(Joint joint); // rho_t from trailer_angle
float rho_c(Joint joint); // rho_c from trailer_angle
float rho_t2theta(Joint joint, float rho_t); // theta_ref from rho_t
void timer_update( timer* tmr);
void read_sensor_values(int* sensor_values, int* _pins);
float calc_sensor_weight(int *min_values, int *max_values, int *sensor_readings, float *weight_values, int* on_line);

int real_ind(Buffer *buff,int arr_ind);
int array_ind(Buffer *buff,int real_ind);
float buffer_mean(Buffer *buff,int len);
void stepper_driver (StepperMotor *motor);
float update_control(Control control, Joint j1, Joint j2);
void update_motor_speed(Control *control, StepperMotor *left_motor, StepperMotor *right_motor);
void timer2_init();
uint8_t data_out[8]= {0}, data_in[MSG_LEN_IN] = {50};
int SensorValues[8] = {0};

float SensorWeight[8] = {0};
float LinePos[3] = {0};
float dLinePos = 0;
float dt = 0.05;
#if TRAILER_NUM==2
#define MAX_STEER_CMD 1.0
#define MAX_SPEED_CMD 0.225
#define MIN_RHO       0.1
int pins[8] = {9,11,10,2,18,19,16,17};
float count2rad = PI/2/355.0;
int SensorMinValues[8] = {250,250,250,250,250,250,250,250};
int SensorMaxValues[8] = {1800,1800,1800,1800,1800,1800,1800,5000};
Joint joints[2] = {
	{.trailer_angle=0,.angle_offset=476,.theta_cmd=0,.Lc=0.102,.Lt=0.076,.K=1},
	{.trailer_angle=0,.angle_offset=499,.theta_cmd=0,.Lc=0.068,.Lt=0.076,.K=0.5}
};
#else
#define MAX_STEER_CMD 1.5
#define MAX_SPEED_CMD 0.25
#define MIN_RHO       0.1
int pins[8] = {16,17,10,18,9,15,11,19};
float count2rad = PI/2/355.0;
int SensorMinValues[8] = {250,250,250,250,250,250,250,250};
int SensorMaxValues[8] = {5000,2100,2100,2100,2000,2000,2100,5000};
Joint joints[2] = {
	{.trailer_angle=0,.angle_offset=540,.theta_cmd=0,.Lc=0.07,.Lt=0.27,.K=2},
	{.trailer_angle=0,.angle_offset=499,.theta_cmd=0,.Lc=0.068,.Lt=0.076,.K=0.5}
};
#endif

int msg_div = 0,MsgDiv = 5, msg_flag = 0;
int debug_div = 0,DebugDiv = 5, debug_flag = 0;
int temp_div = 50, TempDiv = 50, vel_div=0, VelDiv=4;
int ii=0,StepFlag=0;
int last_dir = 0, OnLine=0; // +-1
// x on ramps

StepperMotor LeftMotor = {.enable=0, .Accumulator=0, .Velocity=15, .dir_port=&PORTD, .step_port=&PORTD, .enable_port=&PORTB, .dir_pin=7, .step_pin = 6, .enable_pin=0, .bit_mask=11};

// y on ramps
StepperMotor RightMotor = {.enable=0, .Accumulator=0, .Velocity=15, .dir_port=&PORTD, .step_port=&PORTD, .enable_port=&PORTB, .dir_pin=4, .step_pin = 5, .enable_pin=4, .bit_mask=11};

Control main_control = {.mode=2, // 0-manual 1- auto - line following
	.speed_cmd=0,     // speed command from user
	.steer_cmd=0,     // steer command from user
	.theta_cmd=0,
	.trailer_num=TRAILER_NUM,
	.trailer_angle=0, // measured angle between links
	.Ks=1, // control gain
	.Wc=0.1385,  // distance between car wheels
	.Lc=joints[0].Lc, //0.102, //.Lc=0.07, //0.102,  // car length
	.Lt=joints[0].Lt, //0.103, //.Lt=0.3, //0.103, // trailer length
	.Line_Kp=-2, //-2
	.Line_Kd=-1 //-1
};

InMessage in_message  =  { .steer_cmd=512,.speed_cmd = 512, .mode=2, .update_offset=0};



timer comm_tmr = { .acc=0, .pre=50, .dn=0, .en = 1};
timer on_period_tmr = { .acc=0, .pre=600, .dn=0, .en = 1};
timer off_period_tmr = { .acc=0, .pre=200, .dn=0, .en = 1};
timer off_line_tmr = { .acc=0, .pre=50, .dn=0, .en = 0};
timer on_line_tmr = { .acc=0, .pre=50, .dn=0, .en = 0};
void setup() {
	Serial.begin(57600);
	Serial.println("HHEELLOO");
	Timer1.initialize(50000); // set a timer of length 100000 microseconds (or 0.1 sec - or 10Hz => the led will blink 5 times, 5 cycles of on-and-off, per second)
	Timer1.attachInterrupt(timerIsr); // attach the service routine here
	timer2_init();
	sbi(DDRD,4);
	sbi(DDRD,5);
	sbi(DDRD,6);
	sbi(DDRD,7);
	sbi(DDRB,0);
	sbi(DDRB,4);
	pinMode(3,INPUT);
	sei();
	
}

void loop() {
	while(Serial.available()) {
		char c = Serial.read();
		update_data_in(data_in,c,MSG_LEN_IN);
		ParseInMsg();
	}
	if(debug_flag) {
		Serial.print(main_control.theta_cmd); Serial.write(' ');
		Serial.print(joints[0].trailer_angle); Serial.write(' ');
		//Serial.print(joints[1].trailer_angle); Serial.write(' ');
		//Serial.print(main_control.mode); Serial.write(' ');
		//Serial.print(analogRead(0)); Serial.write(' ' );
		for(ii=0;ii<8;ii++) {
			//Serial.print(SensorValues[ii]); Serial.write(' ');
		}
		Serial.print(LinePos[0]); Serial.write(' ');
		Serial.print(dLinePos); Serial.write(' ');
		Serial.print(OnLine); Serial.write(' ');
		Serial.print(main_control.steer_cmd); Serial.write(' ');
		Serial.print(main_control.speed_cmd); Serial.write(' ');
		Serial.print(main_control.mode); Serial.write(' ');
		Serial.println();
		debug_flag = 0;
	}
	if(StepFlag) {
		timer_update(&comm_tmr);
		timer_update(&on_period_tmr);
		timer_update(&off_period_tmr);
		timer_update(&off_line_tmr);
		timer_update(&on_line_tmr);
		if(on_period_tmr.dn) {
			on_period_tmr.en = 0;
			off_period_tmr.en=1;
			main_control.mode=3;
		}
		if(main_control.mode==3) {
			main_control.speed_cmd-=0.01;
			if(main_control.speed_cmd<=0) {
				main_control.speed_cmd=0;
				main_control.mode=0;
			}
		}
		if(off_period_tmr.dn) {
			off_period_tmr.en = 0;
			on_period_tmr.en=1;
			main_control.mode=2;
		}
		if(off_line_tmr.dn) {
			main_control.mode=0;
		}
		if(on_line_tmr.dn && on_period_tmr.en) {
			main_control.mode=2;
		}
		read_sensor_values(SensorValues,pins);
		vel_div++;
		if(vel_div==VelDiv) {
			vel_div = 0;
			LinePos[2] = LinePos[1];
			LinePos[1] = LinePos[0];
			LinePos[0] = calc_sensor_weight(SensorMinValues,SensorMaxValues,SensorValues,SensorWeight,&OnLine);
			if(OnLine) {
				LinePos[0]>0?last_dir=0.5:last_dir=-0.5;
				off_line_tmr.en = 0;
				on_line_tmr.en = 1;
			}
			else {
				LinePos[0]=last_dir;
				off_line_tmr.en = 1;
				on_line_tmr.en = 0;
			}
			dLinePos = (0*LinePos[2] -1.0*LinePos[1] + 1.0*LinePos[0])/(dt*VelDiv);
		}
		memset(data_out,0,sizeof(data_out));
		if(in_message.update_offset) {
			joints[0].angle_offset = 1023-analogRead(0);
			if(main_control.trailer_num==2) {
				joints[1].angle_offset = analogRead(1);
			}
		}
		if(main_control.mode==0) {
			LeftMotor.enable = 0;
			RightMotor.enable = 0;
		}
		if(main_control.mode>=2){
			LeftMotor.enable = 1;
			RightMotor.enable = 1;
		}
		if(main_control.trailer_num==2) {
			joints[0].trailer_angle = count2rad*(float)((1023-analogRead(0))-joints[0].angle_offset);
			joints[1].trailer_angle = count2rad*(float)(analogRead(1)-joints[1].angle_offset);
		}
		else {
			joints[0].trailer_angle = count2rad*(float)(analogRead(0)-joints[0].angle_offset);
		}
		if(main_control.speed_cmd < 0) {
			main_control.mode=0;
		}
		if(main_control.mode==0) {
			main_control.speed_cmd = 0;//(float)(MSG_SPEED_OFFSET-in_message.speed_cmd)*MSG_SPEED_FACTOR;
			main_control.steer_cmd = 0;//(float)(in_message.steer_cmd-MSG_STEER_OFFSET)*MSG_STEER_FACTOR;
		}
		
		if(main_control.mode >=2) {
			main_control.steer_cmd = main_control.Line_Kp * LinePos[0] + main_control.Line_Kd * dLinePos;
			if(main_control.mode==2 && main_control.speed_cmd<MAX_SPEED_CMD) {
				main_control.speed_cmd+=0.005;
			}
			if(main_control.steer_cmd > MAX_STEER_CMD) {
				main_control.steer_cmd = MAX_STEER_CMD;
			}
			if(main_control.steer_cmd < -MAX_STEER_CMD) {
				main_control.steer_cmd = -MAX_STEER_CMD;
			}
		}
		switch(main_control.mode) {
			case 0: //
			main_control.theta_cmd = main_control.steer_cmd;
			break;
			case 1: // not in use
			case 2: // running
			case 3: // deccelerating
			main_control.theta_cmd = update_control(main_control,joints[0],joints[1]);
			break;
		}
		update_motor_speed(&main_control,&LeftMotor,&RightMotor);
		StepFlag = 0;
	}
}

void timerIsr()
{
	if(++debug_div >= DebugDiv) {
		debug_flag = 1;
		debug_div = 0;
	}
	StepFlag = 1;
}

void timer2_init() {
	PRR &= ~(1 << PRTIM2);

	TCCR2A = (0 << COM2A1) | (0 << COM2A0)   /* Normal port operation, OCA disconnected */
	| (0 << COM2B1) | (0 << COM2B0) /* Normal port operation, OCB disconnected */
	| (1 << WGM21) | (0 << WGM20);  /* TC8 Mode 2 CTC */

	TCCR2B = 0                                          /* TC8 Mode 2 CTC */
	| (1 << CS22) | (0 << CS21) | (0 << CS20); /* IO clock divided by 64 */

	TIMSK2 = 0 << OCIE2B   /* Output Compare B Match Interrupt Enable: disabled */
	| 1 << OCIE2A /* Output Compare A Match Interrupt Enable: enabled */
	| 0 << TOIE2; /* Overflow Interrupt Enable: disabled */
	OCR2A = 40;
	return 0;
}
ISR(TIMER2_COMPA_vect)
{
	stepper_driver(&LeftMotor);
	stepper_driver(&RightMotor);
}
void stepper_driver(StepperMotor *motor) {
	int flag=(motor->Accumulator) & (1<<motor->bit_mask);
	motor->Accumulator += motor->Velocity;
	(motor->Velocity > 0) ? cbi(*(motor->dir_port),motor->dir_pin) : sbi(*(motor->dir_port),motor->dir_pin);
	(flag==(1<<(motor->bit_mask))) ? sbi(*(motor->step_port),motor->step_pin) : cbi(*(motor->step_port),motor->step_pin);
	(motor->enable) ? cbi(*(motor->enable_port),motor->enable_pin) : sbi(*(motor->enable_port),motor->enable_pin) ;
}

float update_control(Control control, Joint j1, Joint j2) {
	float theta_cmd,steer_cmd,theta_cmd1,steer_cmd1;
	float rho_t1_ss = 0,theta1_ss = 0;
	
	if((ABS(j2.trailer_angle))>0.005) {
		rho_t1_ss = rho_c(j2);
		theta1_ss= rho_t2theta(j1,rho_t1_ss);
	}
	else {
		theta1_ss = 0;
	}
	if(control.trailer_num==2) {
		steer_cmd = theta1_ss + j2.K*(j2.trailer_angle-control.steer_cmd);
	}
	else {
		steer_cmd = control.steer_cmd;
	}
	theta_cmd = j1.trailer_angle + j1.K*(j1.trailer_angle-steer_cmd);
	//Serial.println();
	return(theta_cmd);
}

void update_motor_speed(Control *control, StepperMotor *left_motor, StepperMotor *right_motor) {
	float rho=0, w=0, Vl=0,Vr=0;
	if(ABS(control->theta_cmd) > STEER_DEADBAND) {
		// rho is the steady state curvature radius of the car and trailer with theta_cmd angle between
		if(ABS(control->theta_cmd) < MAX_STEER_CMD) {
			rho = (control->Lc*cos(control->theta_cmd)+control->Lt)/sin(control->theta_cmd);
		}
		else {
			(control->theta_cmd)>0?rho=MIN_RHO:rho=-MIN_RHO;
		}
		w = control->speed_cmd/rho;
		Vl = w * (rho + control->Wc/2);
		Vr = w * (rho - control->Wc/2);
	}
	else {
		Vl = control->speed_cmd;
		Vr = control->speed_cmd;
	}
	left_motor->Velocity = (int)(SPEED2STEPPER*Vl);
	right_motor->Velocity = (int)(SPEED2STEPPER*Vr);
}

float rho_c(Joint joint) {
	return(joint.Lc*cos(joint.trailer_angle)+joint.Lt)/sin(joint.trailer_angle);
}

float rho_t(Joint joint) {
	return(joint.Lt*cos(joint.trailer_angle)+joint.Lc)/sin(joint.trailer_angle);
}

float rho_t2theta(Joint joint, float rho_t) {
	float Lc = joint.Lc;
	float Lt = joint.Lt;
	float rho_c = sqrt(rho_t*rho_t + Lt*Lt - Lc*Lc);
	if(rho_t < 0) {
		rho_c = -rho_c;
	}
	return(atan(Lt/rho_t) + atan(Lc/rho_c));
}
void update_data_in(uint8_t *data, uint8_t val, int len) {
	int ii = 0;
	for(ii=0;ii<(len-1);ii++) {
		data[ii] = data[ii+1];
	}
	data[len-1] = val;
}
void ParseInMsg() {
	uint8_t speed_str[4] = {0};
	uint8_t steer_str[4] = {0};
	if(data_in[0]=='S' && data_in[12]=='E') {
		memcpy(speed_str,&data_in[1],3);
		memcpy(steer_str,&data_in[5],3);
		in_message.steer_cmd = atoi(steer_str);
		in_message.speed_cmd = atoi(speed_str);
		in_message.mode = data_in[9]-'0';
		in_message.update_offset = data_in[11]-'0';
		memset(data_in,0,sizeof(data_in));
		comm_tmr.acc = 0;
	}
}

void timer_update( timer* tmr) {
	if(tmr->en)  {
		if (tmr->acc < tmr->pre) {
			tmr->acc++;
			tmr->dn = 0;
		}
		else {
			tmr->dn = 1;
		}
	}
	else {
		tmr->acc = 0;
		tmr->dn = 0;
	}
}

void read_sensor_values(int* sensor_values, int* _pins) {
	int i=0;
	for(i = 0; i < 8; i++)
	{
		sensor_values[i] = 5000;
		digitalWrite(_pins[i], HIGH);   // make sensor line an output
		pinMode(_pins[i], OUTPUT);      // drive sensor line high
	}

	delayMicroseconds(10);              // charge lines for 10 us

	for(i = 0; i < 8; i++)
	{
		pinMode(_pins[i], INPUT);       // make sensor line an input
		digitalWrite(_pins[i], LOW);        // important: disable internal pull-up!
	}

	unsigned long startTime = micros();
	while (micros() - startTime < 2500)
	{
		unsigned int time = micros() - startTime;
		for (i = 0; i < 8; i++)
		{
			if (digitalRead(_pins[i]) == LOW && time < sensor_values[i])
			sensor_values[i] = time;
		}
	}
}

float calc_sensor_weight(int *min_values, int *max_values, int *sensor_readings, float *weight_values, int* on_line) {
	int ii=0,temp_on_line=0;
	float weight=0,weight_sum=0, pos_sum=0;
	for(ii=0;ii<8;ii++) {
		weight = (float)(sensor_readings[ii]-min_values[ii])/(float)(max_values[ii]-min_values[ii]);
		if(weight<0) {
			weight=0;
		}
		if(weight>1) {
			weight=1;
		}
		weight_values[ii] = weight;
		if(weight>0.2) {
			temp_on_line = 1;
			weight_sum += weight;
			pos_sum += weight*(ii);
		}
	}
	if(temp_on_line) {
		*on_line=1;
		return(pos_sum)/(weight_sum*7)-0.5;
	}
	*on_line=0;
	return(100);
}

