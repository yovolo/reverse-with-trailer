void buffer_update(Buffer *buff,float new_val) {
  buff->ind = (buff->ind+buff->len-1)%buff->len; 
  buff->array[buff->ind] = new_val;
  buff->mean = mean(buff->array,buff->len);
  buff->std = mystd(buff->array,buff->len);
}

int real_ind(Buffer *buff,int arr_ind) {
  int ret = arr_ind-buff->ind;
    if(ret<0)
      ret += buff->len;
    return(ret);
}

int array_ind(Buffer *buff,int real_ind) {
  int ret = real_ind + buff->ind;
  if(ret >= buff->len)
    ret -= buff->len;
  return(ret);
}

float buffer_mean(Buffer *buff,int len) {
  float mean = 0.0f;
  for(int ii=0;ii<len;ii++) {
    mean += buff->array[array_ind(buff,ii)];
  }
  return(mean/len);
}

float tf_filter(float *coef_b, float *coef_a, Buffer *input, Buffer *output,int len) {
  /*
  tf_filter performs a one step update for a transfer function.
  coef_b is the transfer function nominator coefficients vector.
  coef_a is the transfer funnction denominator coefficients vector.
  The function assumes input and output buffers are synchronized, while output[0] is to be computed here.
  */
  float new_val = 0.0f,temp_in=0,temp_out=0;
  output->array[output->ind] = 0;
  for(int ii=0;ii<len;ii++) {
    temp_in = input->array[array_ind(input,ii)];
    temp_out = output->array[array_ind(output,ii)];
    new_val += (coef_b[ii]*temp_in - coef_a[ii]*temp_out);
  }
  output->array[output->ind] = new_val / coef_a[0];
}


